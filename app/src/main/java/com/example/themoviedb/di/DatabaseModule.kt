package com.example.themoviedb.di

import android.content.Context
import androidx.room.Room
import com.example.themoviedb.data.db.DB_NAME
import com.example.themoviedb.data.db.DatabaseClient
import com.example.themoviedb.data.db.ShowDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    fun provideDao(database: DatabaseClient): ShowDao {
        return database.showDAO()
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): DatabaseClient {
        return Room.databaseBuilder(
            context,
            DatabaseClient::class.java,
            DB_NAME
        ).fallbackToDestructiveMigration().build()
    }

}