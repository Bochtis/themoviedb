package com.example.themoviedb.data.models

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    val page: Int? = null,
    @SerializedName("total_results")
    val totalResults: Int? = null,
    @SerializedName("total_pages")
    val totalPages: Int? = null,
    @SerializedName("results")
    val shows: List<Show>? = null
)