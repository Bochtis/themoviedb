package com.example.themoviedb.data.rest

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ServiceGenerator {

    val API_BASE_URL = "https://api.themoviedb.org/3/"
    val API_BASE_IMG_URL = "https://image.tmdb.org/t/p/original"

    val API_BASE_YOUTUBE_URL = "http://img.youtube.com/vi/"
    val API_BASE_YOUTUBE_SUFIX = "/0.jpg"

    val API_KEY = "6b2e856adafcc7be98bdf0d8b076851c"
    val API_LANGUAGE = "en-US"

    val httpLoggingInterceptor = HttpLoggingInterceptor()
    val httpClient = OkHttpClient.Builder()

    val builder = Retrofit.Builder()

    var retrofit: Retrofit

    init {
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(httpLoggingInterceptor)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)

        builder
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

        retrofit = builder
            .client(httpClient.build())
            .build()
    }

    fun <T> createService(serviceClass: Class<T>): T {
        val service = retrofit.create(serviceClass)
        return service as T
    }

}