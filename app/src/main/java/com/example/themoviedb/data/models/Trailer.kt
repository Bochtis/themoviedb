package com.example.themoviedb.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Trailer(
    val id: String? = null,
    @SerializedName("iso_639_1")
    val iso6391: String? = null,
    @SerializedName("iso_3166_1")
    val iso31661: String? = null,
    val key: String? = null,
    val name: String? = null,
    val site: String? = null,
    val size: Int? = null,
    val type: String? = null
) : Parcelable