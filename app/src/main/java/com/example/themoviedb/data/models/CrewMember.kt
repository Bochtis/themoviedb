package com.example.themoviedb.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CrewMember(
    @SerializedName("credit_id")
    val creditId: String,
    val department: String,
    val gender: Int? = null,
    val id: Int,
    val job: String,
    val name: String,
    @SerializedName("profile_path")
    val profilePath: String? = null
) : Parcelable