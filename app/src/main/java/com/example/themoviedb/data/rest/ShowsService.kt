package com.example.themoviedb.data.rest

import com.example.themoviedb.data.models.CreditsResponse
import com.example.themoviedb.data.models.SearchResponse
import com.example.themoviedb.data.models.Show
import com.example.themoviedb.data.models.TrailersResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ShowsService {

    @GET("search/multi?")
    suspend fun getShows(@Query("api_key") apiKey: String, @Query("query") query: String, @Query("page") page: String): Response<SearchResponse>

    @GET("movie/now_playing")
    suspend fun getNowPlayingShows(@Query("api_key") apiKey: String, @Query("page") page: String): Response<SearchResponse>

    @GET("{type}/{id}")
    suspend fun getShow(@Path("type") type: String, @Path("id") id: String, @Query("api_key") apiKey: String): Response<Show>

    @GET("movie/{movie_id}")
    suspend fun getMovie(@Path("movie_id") movieId: Int, @Query("api_key") apiKey: String): Response<Show>

    @GET("tv/{tv_id}")
    suspend fun getShow(@Path("tv_id") tvId: Int, @Query("api_key") apiKey: String): Response<Show>

    @GET("movie/{movie_id}/credits")
    suspend fun getMovieCredits(@Path("movie_id") movieId: Int, @Query("api_key") apiKey: String, @Query("language") language: String): Response<CreditsResponse>

    @GET("tv/{tv_id}/credits")
    suspend fun getTVCredits(@Path("tv_id") tvId: Int, @Query("api_key") apiKey: String, @Query("language") language: String): Response<CreditsResponse>

    @GET("movie/{movie_id}/videos")
    suspend fun getTrailers(@Path("movie_id") movieId: Int, @Query("api_key") apiKey: String, @Query("language") language: String): Response<TrailersResponse>
}