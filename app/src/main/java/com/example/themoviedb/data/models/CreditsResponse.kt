package com.example.themoviedb.data.models

data class CreditsResponse(
    val cast: List<Actor>? = null,
    val crew: List<CrewMember>? = null,
    val id: Int
)