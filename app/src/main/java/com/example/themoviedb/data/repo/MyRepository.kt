package com.example.themoviedb.data.repo

import androidx.annotation.WorkerThread
import com.example.themoviedb.data.db.ShowDao
import com.example.themoviedb.data.models.Show
import com.example.themoviedb.data.rest.ServiceGenerator
import com.example.themoviedb.data.rest.ShowsService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MyRepository @Inject constructor(
    private val service: ShowsService,
    private val dao: ShowDao
) {

    /**
     * REST API
     */
    suspend fun searchShows(query: String) = service.getShows(ServiceGenerator.API_KEY, query, "1")

    suspend fun getNowPlayingShows() = service.getNowPlayingShows(ServiceGenerator.API_KEY, "1")

    suspend fun getMovie(movieId: Int) = service.getMovie(movieId, ServiceGenerator.API_KEY)

    suspend fun getShow(tvId: Int) = service.getShow(tvId, ServiceGenerator.API_KEY)

    suspend fun getMovieCredits(movieId: Int) =
        service.getMovieCredits(movieId, ServiceGenerator.API_KEY, ServiceGenerator.API_LANGUAGE)

    suspend fun getTVCredits(tvId: Int) =
        service.getTVCredits(tvId, ServiceGenerator.API_KEY, ServiceGenerator.API_LANGUAGE)

    suspend fun getTrailers(movieId: Int) =
        service.getTrailers(movieId, ServiceGenerator.API_KEY, ServiceGenerator.API_LANGUAGE)

    /**
     * ROOM DATABASE
     */
    val watchList: Flow<List<Show>> = dao.getShows()

    suspend fun showIsSaved(showId: Int) = dao.isShowSaved(showId)

    fun searchWatchListShows(query: String)  = dao.getShowsByName(query)

    @WorkerThread
    suspend fun addShowToWatchList(show: Show) {
        dao.addShow(show)
    }
}