package com.example.themoviedb.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Season(
    @SerializedName("air_date")
    var airDate: String? = null,
    @SerializedName("episode_count")
    var episodeCount: Int? = null,
    var name: String? = null
) : Parcelable