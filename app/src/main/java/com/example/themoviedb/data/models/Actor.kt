package com.example.themoviedb.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Actor(
    @SerializedName("cast_id")
    val castId: Int? = null,
    val character: String,
    @SerializedName("credit_id")
    val creditId: String,
    val gender: Int? = null,
    val id: Int,
    val name: String,
    val order: Int,
    @SerializedName("profile_path")
    val profilePath: String? = null
) : Parcelable