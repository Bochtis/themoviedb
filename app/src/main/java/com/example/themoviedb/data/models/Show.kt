package com.example.themoviedb.data.models

import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "shows")
@TypeConverters(Converters::class)
@Parcelize
data class Show(
    @PrimaryKey
    @ColumnInfo
    var id: Int? = null,
    @SerializedName("backdrop_path")
    @ColumnInfo
    var backdropPath: String? = null,
    @SerializedName("poster_path")
    @ColumnInfo
    var posterPath: String? = null,
    @SerializedName("media_type")
    @ColumnInfo
    var mediaType: String? = null,
    @SerializedName("original_title")
    @ColumnInfo
    var originalTitle: String? = null,
    @SerializedName("release_date")
    @ColumnInfo
    var releaseDate: String? = null,
    @SerializedName("vote_average")
    @ColumnInfo
    var voteAverage: Double? = null,
    @SerializedName("first_air_date")
    @ColumnInfo
    var firstAirDate: String? = null,
    @SerializedName("original_name")
    @ColumnInfo
    var originalName: String? = null,
    @ColumnInfo
    var name: String? = null,
    @ColumnInfo
    var seasons: List<Season>? = null,
    @ColumnInfo
    var title: String? = null,
    @ColumnInfo
    var video: Boolean? = null,
    @ColumnInfo
    var overview: String? = null,
    @ColumnInfo
    var genres: List<Genre>? = null,
    @ColumnInfo
    var cast: List<Actor>? = null,
    @ColumnInfo
    var crew: List<CrewMember>? = null,
    @ColumnInfo
    var trailers: List<Trailer>? = null
) : Parcelable