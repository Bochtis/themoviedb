package com.example.themoviedb.data.models

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    // SEASON
    @TypeConverter
    fun stringToSeasonList(value: String?): List<Season> {
        if (value == null){
            return listOf()
        }
        val listType = object : TypeToken<List<Season>>() {

        }.type
        return Gson().fromJson<List<Season>>(value, listType)
    }

    @TypeConverter
    fun seasonListToString(list: List<Season>?): String? {
        if (list == null){
            return null
        }
        return Gson().toJson(list)
    }


    // GENRE
    @TypeConverter
    fun stringToGenreList(value: String?): List<Genre> {
        if (value == null){
            return listOf()
        }
        val listType = object : TypeToken<List<Genre>>() {

        }.type
        return Gson().fromJson<List<Genre>>(value, listType)
    }

    @TypeConverter
    fun genreListToString(list: List<Genre>?): String? {
        if (list == null){
            return null
        }
        return Gson().toJson(list)
    }


    // CAST
    @TypeConverter
    fun stringToCastList(value: String?): List<Actor> {
        if (value == null){
            return listOf()
        }
        val listType = object : TypeToken<List<Actor>>() {

        }.type
        return Gson().fromJson<List<Actor>>(value, listType)
    }

    @TypeConverter
    fun castListToString(list: List<Actor>?): String? {
        if (list == null){
            return null
        }
        return Gson().toJson(list)
    }

    // CREW
    @TypeConverter
    fun stringToCrewList(value: String?): List<CrewMember> {
        if (value == null){
            return listOf()
        }
        val listType = object : TypeToken<List<CrewMember>>() {

        }.type
        return Gson().fromJson<List<CrewMember>>(value, listType)
    }

    @TypeConverter
    fun crewListToString(list: List<CrewMember>?): String? {
        if (list == null){
            return null
        }
        return Gson().toJson(list)
    }

    // TRAILER
    @TypeConverter
    fun stringToTrailerList(value: String?): List<Trailer> {
        if (value == null){
            return listOf()
        }
        val listType = object : TypeToken<List<Trailer>>() {

        }.type
        return Gson().fromJson<List<Trailer>>(value, listType)
    }

    @TypeConverter
    fun trailerListToString(list: List<Trailer>?): String? {
        if (list == null){
            return null
        }
        return Gson().toJson(list)
    }
}