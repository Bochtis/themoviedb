package com.example.themoviedb.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.themoviedb.data.models.Show
import kotlinx.coroutines.flow.Flow

@Dao
interface ShowDao {

    @Insert
    suspend fun addShow(show: Show)

    @Query("SELECT * FROM shows")
    fun getShows(): Flow<List<Show>>

    @Query("SELECT EXISTS(SELECT * FROM shows WHERE id = :showId)")
    suspend fun isShowSaved(showId: Int): Boolean

    @Query("SELECT * FROM shows WHERE originalName LIKE :query OR originalTitle LIKE :query")
    fun getShowsByName(query: String): Flow<List<Show>>

}