package com.example.themoviedb.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.themoviedb.data.models.Show

const val DATABASE_SCHEMA_VERSION = 3
const val DB_NAME = "shows_db"

@Database(version = DATABASE_SCHEMA_VERSION, entities = [Show::class])
abstract class DatabaseClient : RoomDatabase() {

    // Insert DAO below
    abstract fun showDAO(): ShowDao

}