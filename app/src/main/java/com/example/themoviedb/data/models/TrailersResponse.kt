package com.example.themoviedb.data.models

import com.google.gson.annotations.SerializedName

data class TrailersResponse(
    val id: Int? = null,
    @SerializedName("results")
    var trailers: List<Trailer>? = null
)