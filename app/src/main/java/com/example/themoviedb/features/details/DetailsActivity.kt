package com.example.themoviedb.features.details

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.R
import com.example.themoviedb.data.models.Actor
import com.example.themoviedb.data.models.CrewMember
import com.example.themoviedb.data.models.Show
import com.example.themoviedb.data.models.Trailer
import com.example.themoviedb.data.rest.ServiceGenerator
import com.example.themoviedb.databinding.ActivityDetailsBinding
import com.example.themoviedb.utils.Extras
import com.example.themoviedb.utils.setImage
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.cast_layout.*
import kotlinx.android.synthetic.main.movie_item.view.*
import kotlinx.android.synthetic.main.overview_layout.*
import kotlinx.android.synthetic.main.trailers_layout.*

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetailsBinding

    var id: Int? = null
    var type: String? = null
    var isAddedToWatchlist = false
    lateinit var castAdapter: CastRecyclerAdapter
    lateinit var trailersAdapter: TrailersRecyclerAdapter

    private val viewModel: DetailsViewModel by viewModels()

    var show: Show? = null

    companion object {
        // View name of the poster image. Used for activity scene transitions
        val VIEW_NAME_POSTER_IMAGE = "detail:poster:image"

        val TYPE_MOVIE = "movie"
        val TYPE_TV_SHOW = "tv"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)

        // get intent extras
        intent.extras?.let { bundle ->
            show = bundle.getParcelable(Extras.SHOW)
            type = show?.mediaType
            id = show?.id
        }

        ViewCompat.setTransitionName(showPosterImgView, VIEW_NAME_POSTER_IMAGE)
        initViews()
        observeViewModel()
    }

    private fun initViews() {
        // load show data
        show?.let {
            updateViews(it)
        }

        // Make the activity fullscreen
        WindowCompat.setDecorFitsSystemWindows(window, false)
        window.statusBarColor = getColor(R.color.colorHighTransparentDark)

        // SET THE TOOLBAR
        setSupportActionBar(materialToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // set listeners
        watchListBtn.setOnClickListener {
            show?.let { show ->
                if (!isAddedToWatchlist) {
                    viewModel.addToWatchList(show)
                    displayDialog(
                        "Success!",
                        "You have successfully saved this show to your watchlist.",
                        "ok"
                    )
                    configureFloatingButton(watchListBtn, true)
                }
            }
        }

        var isToolbarShown = false
        // scroll change listener begins at Y = 0 when image is fully collapsed
        nestedScrollView.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->
                // User scrolled past image to height of toolbar and the title text is
                // underneath the toolbar, so the toolbar should be shown.
                val shouldShowToolbar = scrollY > materialToolbar.height

                // The new state of the toolbar differs from the previous state; update
                // appbar and toolbar attributes.
                if (isToolbarShown != shouldShowToolbar) {
                    isToolbarShown = shouldShowToolbar

                    // Use shadow animator to add elevation if toolbar is shown
                    appBarLayout.isActivated = shouldShowToolbar

                    // Show the plant name if toolbar is shown
                    toolbarLayout.isTitleEnabled = shouldShowToolbar
                }
            }
        )

        // SET UP THE CAST ADAPTER
        castAdapter =
            CastRecyclerAdapter(mutableListOf())
        val cManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        castRecycler.apply {
            setHasFixedSize(true)
            layoutManager = cManager
            adapter = castAdapter
        }

        // SET UP THE TRAILERS ADAPTER
        trailersAdapter =
            TrailersRecyclerAdapter(this, mutableListOf())
        val tManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        trailerRecycler.apply {
            setHasFixedSize(true)
            layoutManager = tManager
            adapter = trailersAdapter
        }
    }

    private fun observeViewModel() {
        viewModel.loading.observe(this) { loading ->
            if (loading)
                progressBarRel.visibility = View.VISIBLE
            else
                progressBarRel.visibility = View.GONE
        }

        viewModel.show.observe(this) { show ->
            this.show = show
            /**
             * we need to set the media type of the show, because the api request(show details) doesn't provide it
             */
            show.mediaType = type
            updateViews(show)
        }

        viewModel.cast.observe(this) { cast ->
            setCast(cast)
        }

        viewModel.crew.observe(this) { crew ->
            setCrew(crew)
        }

        viewModel.trailers.observe(this) { trailers ->
            setTrailers(trailers)
        }

        viewModel.showLoadError.observe(this) { error ->
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        }

        viewModel.isAddedToWatchList.observe(this) { status ->
            isAddedToWatchlist = status
            configureFloatingButton(watchListBtn, isAddedToWatchlist)
        }
    }

    override fun onResume() {
        super.onResume()
        fetchData()
    }

    private fun fetchData() {
        viewModel.fetchShow(type, id)
        viewModel.fetchCastAndCrew(type, id)
        if (type == TYPE_MOVIE)
            viewModel.fetchTrailers(id)
    }

    /**
     * HELPER FUNCTIONS
     */
    private fun updateViews(show: Show) {
        /**
         * We need to ask the view model to check if the current show is already added in watchlist.
         * Whether we clicked on a show of our watchlist, or the api search results list, the view model
         * must inform this activity about the status of the show.
         */
        show.id?.let { viewModel.checkIfShowIsAddedInWatchList(it) }

        // LOAD IMAGES
        show.backdropPath.let {
            logoImgView.setImage(
                String.format(
                    "%s%s",
                    ServiceGenerator.API_BASE_IMG_URL,
                    show.backdropPath
                )
            )
        }

        show.posterPath.let {
            showPosterImgView.setImage(
                String.format(
                    "%s%s",
                    ServiceGenerator.API_BASE_IMG_URL,
                    show.posterPath
                )
            )
        }

        // SET THE TITLE
        titleTxtView.text = if (type == TYPE_TV_SHOW) {
            show.originalName
        } else {
            show.originalTitle
        }
        binding.toolbarLayout.title = titleTxtView.text

        // SET THE GENRE
        if (!show.genres.isNullOrEmpty()) {
            genresTxtView.text = show.genres!![0].name
        }

        // SET THE SUMMARY
        summaryValueTxtView.text = show.overview
    }

    private fun setCast(cast: List<Actor>?) {
        if (!cast.isNullOrEmpty()) {
            show?.cast = cast
            castDividerView.visibility = View.VISIBLE
            castLayout.visibility = View.VISIBLE
            castAdapter.cast.clear()
            castAdapter.cast.addAll(cast)
            castAdapter.notifyDataSetChanged()
        }
    }

    private fun setCrew(crew: List<CrewMember>?) {
        if (!crew.isNullOrEmpty()) {
            show?.crew = crew
            run {
                crew.forEach { crewMember ->
                    if (crewMember.job == "Director") {
                        directorLin.visibility = View.VISIBLE
                        directorTxtView.text = crewMember.name
                        return@run
                    }
                }

                crew.forEach { crewMember ->
                    if (crewMember.department == "Writing") {
                        writersLin.visibility = View.VISIBLE
                        if (writersTxtView.text.isEmpty())
                            writersTxtView.text = crewMember.name
                        else
                            writersTxtView.text =
                                String.format("%s, %s", writersTxtView.text, crewMember.name)
                    }
                }
            }
        }
    }

    private fun setTrailers(trailers: List<Trailer>?) {
        if (!trailers.isNullOrEmpty()) {
            show?.trailers = trailers
            trailersDividerView.visibility = View.VISIBLE
            trailersLayout.visibility = View.VISIBLE
            trailersAdapter.trailers.clear()
            trailersAdapter.trailers.addAll(trailers)
            trailersAdapter.notifyDataSetChanged()
        }
    }

    private fun configureFloatingButton(button: FloatingActionButton, isDisabled: Boolean) {
        if (isDisabled) {
            button.backgroundTintList =
                ColorStateList.valueOf(
                    ContextCompat.getColor(
                        this@DetailsActivity,
                        R.color.colorPrimaryDark
                    )
                )
            isAddedToWatchlist = true
        }
    }

    private fun displayDialog(title: String, message: String, button: String) {
        MaterialAlertDialogBuilder(this@DetailsActivity)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(
                button
            ) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    /**
     * HANDLE BACK BUTTON
     */
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
