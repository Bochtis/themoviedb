package com.example.themoviedb.features.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.R
import com.example.themoviedb.data.models.Actor
import com.example.themoviedb.data.rest.ServiceGenerator
import com.example.themoviedb.utils.setImage
import kotlinx.android.synthetic.main.cast_item.view.*

class CastRecyclerAdapter(var cast: MutableList<Actor> = mutableListOf()) :
    RecyclerView.Adapter<CastRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cast_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount() = cast.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(cast[position])
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(actor: Actor) {
            val url: String? = actor.profilePath
            url?.let {
                itemView.personImgView.setImage(
                    String.format(
                        "%s%s",
                        ServiceGenerator.API_BASE_IMG_URL,
                        url
                    )
                )
            }
            itemView.personNameTxtView.text = actor.character
            itemView.characterNameTxtView.text = actor.name
        }

    }
}