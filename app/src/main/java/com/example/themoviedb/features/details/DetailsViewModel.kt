package com.example.themoviedb.features.details

import androidx.lifecycle.*
import com.example.themoviedb.data.models.*
import com.example.themoviedb.data.repo.MyRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(private val repository: MyRepository) : ViewModel() {

    val show = MutableLiveData<Show>()
    val showLoadError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val cast = MutableLiveData<List<Actor>?>()
    val crew = MutableLiveData<List<CrewMember>?>()
    val trailers = MutableLiveData<List<Trailer>?>()
    val isAddedToWatchList = MutableLiveData<Boolean>()

    fun fetchShow(type: String?, id: Int?) {
        if (id != null) {
            loading.value = true
            viewModelScope.launch {
                kotlin.runCatching {
                    when (type) {
                        DetailsActivity.TYPE_TV_SHOW -> repository.getShow(id)
                        else -> repository.getMovie(id)
                    }
                }.onFailure {
                    loading.value = false
                    showLoadError.value = it.message
                }.onSuccess {
                    loading.value = false
                    if (it.isSuccessful) {
                        show.postValue(it.body())
                    }
                }
            }
        } else {
            showLoadError.value = "Show id should not be null.."
        }
    }

    fun fetchCastAndCrew(type: String?, id: Int?) {
        if (id != null) {
            loading.value = true
            viewModelScope.launch {
                kotlin.runCatching {
                    when (type) {
                        DetailsActivity.TYPE_TV_SHOW -> repository.getTVCredits(id)
                        else -> repository.getMovieCredits(id)
                    }
                }.onFailure {
                    loading.value = false
                    showLoadError.value = it.message
                }.onSuccess {
                    loading.value = false
                    if (it.isSuccessful) {
                        cast.postValue(it.body()?.cast)
                        crew.postValue(it.body()?.crew)
                    }
                }
            }
        } else {
            showLoadError.value = "Show id should not be null.."
        }
    }

    fun fetchTrailers(movieId: Int?) {
        if (movieId != null) {
            loading.value = true
            viewModelScope.launch {
                kotlin.runCatching {
                    repository.getTrailers(movieId)
                }.onFailure {
                    loading.value = false
                    showLoadError.value = it.message
                }.onSuccess {
                    loading.value = false
                    if (it.isSuccessful) {
                        trailers.postValue(it.body()?.trailers)
                    }
                }
            }
        } else {
            showLoadError.value = "Show id should not be null.."
        }
    }

    fun addToWatchList(show: Show) {
        viewModelScope.launch {
            repository.addShowToWatchList(show)
        }
    }

    fun checkIfShowIsAddedInWatchList(showId: Int) {
        viewModelScope.launch {
            isAddedToWatchList.postValue(repository.showIsSaved(showId))
        }
    }
}