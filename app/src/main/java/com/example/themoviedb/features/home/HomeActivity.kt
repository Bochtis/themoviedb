package com.example.themoviedb.features.home

import android.content.Intent
import android.graphics.Outline
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.EditText
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.util.Pair
import androidx.core.view.WindowCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviedb.R
import com.example.themoviedb.data.models.Show
import com.example.themoviedb.data.rest.ServiceGenerator
import com.example.themoviedb.databinding.ActivityHomeBinding
import com.example.themoviedb.features.details.DetailsActivity
import com.example.themoviedb.utils.Extras
import com.example.themoviedb.utils.setImage
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    lateinit var binding: ActivityHomeBinding
    lateinit var showsRecyclerAdapter: ShowsRecyclerAdapter
    lateinit var search: SearchView

    private val viewModel: HomeViewModel by viewModels()

    companion object {
        var watchListIsDisplayed = false
        var isSearchViewExpanded = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        initViews()
        fetchData()
        observeViewModel()
    }

    private fun initViews() {
        // Make the activity fullscreen
        WindowCompat.setDecorFitsSystemWindows(window, false)
        window.statusBarColor = getColor(R.color.colorHighTransparentDark)

        // SET UP TOOLBAR
        if (isSearchViewExpanded)
            binding.titleTxtView.visibility = View.GONE

        binding.searchView.setOnSearchClickListener {
            binding.titleTxtView.visibility = View.GONE
            isSearchViewExpanded = true
        }

        binding.searchView.setOnCloseListener {
            binding.titleTxtView.visibility = View.VISIBLE
            isSearchViewExpanded = false
            false
        }

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean = false

            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    val qr = "%$it%"
                    viewModel.fetchWatchListShows(qr)
                }
                return true
            }
        })

        val editText =
            binding.searchView.findViewById<View>(R.id.search_src_text) as EditText
        editText.setTextColor(ContextCompat.getColor(this, R.color.home_secondary_text))

        // SET UP THE RECYCLER VIEW
        showsRecyclerAdapter = ShowsRecyclerAdapter { show, view ->
            displayDetailsForTheShow(show, view)
        }
        val layoutManager = LinearLayoutManager(this)

        binding.recyclerView.apply {
            setLayoutManager(layoutManager)
            setHasFixedSize(true)
            adapter = showsRecyclerAdapter
        }

        // set header corners
        val curveRadius = 100F
        binding.headerView.headerImgView.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View?, outline: Outline?) {
                outline?.setRoundRect(
                    0,
                    -curveRadius.toInt(),
                    view!!.width,
                    view.height,
                    curveRadius
                )
            }
        }
        binding.headerView.headerImgView.clipToOutline = true
    }

    private fun fetchData() {
        viewModel.getNowPlayingShows()
    }

    private fun observeViewModel() {
        viewModel.loading.observe(this) { loading ->
            binding.progressBar.visibility = if (loading)
                View.VISIBLE
            else
                View.INVISIBLE
        }

        viewModel.nowPlayingShows.observe(this) { shows ->
            shows?.let { updateShowsAdapter(it) }
        }

        viewModel.showsLoadError.observe(this) { error ->
            Log.d("SearchActivity: ", error)
        }
    }

    /**
     * HELPER FUNCTIONS
     */
    private fun displayDetailsForTheShow(show: Show, view: View) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(Extras.SHOW, show)

        val activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            Pair(
                view.findViewById(R.id.showImgView),
                DetailsActivity.VIEW_NAME_POSTER_IMAGE
            )
        )

        ActivityCompat.startActivity(this, intent, activityOptionsCompat.toBundle())
    }

    private fun updateShowsAdapter(shows: List<Show>?) {
        showsRecyclerAdapter.updateShows(shows ?: listOf())
        if (shows.isNullOrEmpty()) {
            binding.searchTxtView.visibility = View.VISIBLE
        } else {
            binding.searchTxtView.visibility = View.INVISIBLE
            binding.headerView.headerImgView.setImage(
                String.format(
                    "%s%s",
                    ServiceGenerator.API_BASE_IMG_URL,
                    shows.random().posterPath
                )
            )
        }
    }
}
