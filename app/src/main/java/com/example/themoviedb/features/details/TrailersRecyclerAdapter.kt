package com.example.themoviedb.features.details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.R
import com.example.themoviedb.data.models.Trailer
import com.example.themoviedb.data.rest.ServiceGenerator
import com.example.themoviedb.utils.setImage
import kotlinx.android.synthetic.main.trailer_item.view.*

class TrailersRecyclerAdapter(
    val context: Context,
    var trailers: MutableList<Trailer> = mutableListOf()
) : RecyclerView.Adapter<TrailersRecyclerAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.trailer_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(trailers[position])
    }

    override fun getItemCount(): Int = trailers.size

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var player: PlayerManager

        fun bind(trailer: Trailer) {
            itemView.trailerTitle.text = trailer.name

            if (trailer.key != null)
                itemView.imageView.setImage("${ServiceGenerator.API_BASE_YOUTUBE_URL}${trailer.key}${ServiceGenerator.API_BASE_YOUTUBE_SUFIX}")

            itemView.imageView.visibility = View.VISIBLE
            itemView.playerView.visibility = View.INVISIBLE
            itemView.playBtn.visibility = View.VISIBLE

            itemView.playBtn.setOnClickListener { view ->
                itemView.imageView.visibility = View.INVISIBLE
                itemView.playerView.visibility = View.VISIBLE
                view.visibility = View.INVISIBLE

                if (trailer.key != null)
                /*itemView.youtubeVideo.initialize(
                    object : AbstractYouTubePlayerListener() {
                        override fun onReady(youTubePlayer: YouTubePlayer) {
                            youTubePlayer.loadVideo(
                                trailer.key,
                                0F
                            )
                        }
                    }, true
                )*/
                    initializePlayer()
            }
        }

        private fun initializePlayer(){
            player = PlayerManager(context)
            player.init(context, itemView.playerView)
        }
    }
}