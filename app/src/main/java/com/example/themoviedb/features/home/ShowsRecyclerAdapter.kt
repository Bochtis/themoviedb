package com.example.themoviedb.features.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedb.R
import com.example.themoviedb.data.models.Show
import com.example.themoviedb.data.rest.ServiceGenerator
import com.example.themoviedb.utils.setImage
import kotlinx.android.synthetic.main.movie_item.view.*


class ShowsRecyclerAdapter(
    var shows: MutableList<Show> = mutableListOf(),
    private val listener: (Show, View) -> Unit
) : RecyclerView.Adapter<ShowsRecyclerAdapter.MyViewHolder>() {

    private val type_movie = "movie"
    private val type_tv_show = "tv"

    fun updateShows(shows: List<Show>){
        this.shows.clear()
        this.shows.addAll(shows)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(shows[position], listener)
    }

    override fun getItemCount(): Int = shows.size

    /**
     * VIEW HOLDERS
     */
    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(show: Show, listener: (Show, View) -> Unit) {
            itemView.container.setOnClickListener {
                listener(show, itemView)
            }

            itemView.titleTxtView.text = if (show.mediaType == type_tv_show)
                show.originalName
            else
                show.originalTitle

            show.overview?.let { overview ->
                itemView.summaryTxtView.text = overview
            }

            itemView.ratingTxtView.text = String.format("%s/10", show.voteAverage)

            itemView.showImgView.setImage(
                String.format(
                    "%s%s",
                    ServiceGenerator.API_BASE_IMG_URL,
                    show.posterPath
                )
            )
        }
    }
}