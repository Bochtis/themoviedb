package com.example.themoviedb.features.home

import androidx.lifecycle.*
import com.example.themoviedb.data.models.Show
import com.example.themoviedb.data.repo.MyRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: MyRepository) : ViewModel() {

    val shows: LiveData<List<Show>> = repository.watchList.asLiveData()
    lateinit var searchResults: LiveData<List<Show>>
    val nowPlayingShows = MutableLiveData<List<Show>?>()
    val showsLoadError = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()

    fun fetchWatchListShows(query: String) {
        searchResults = repository.searchWatchListShows(query).asLiveData()
    }

    fun getNowPlayingShows() {
        loading.value = true
        viewModelScope.launch {
            kotlin.runCatching {
                repository.getNowPlayingShows()
            }.onFailure {
                loading.value = false
                showsLoadError.value = it.message
            }.onSuccess {
                loading.value = false
                if (it.isSuccessful) {
                    nowPlayingShows.postValue(it.body()?.shows)
                }
            }
        }
    }
}