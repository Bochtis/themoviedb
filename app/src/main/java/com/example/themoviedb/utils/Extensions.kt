package com.example.themoviedb.utils

import android.content.res.Resources.getSystem
import android.widget.ImageView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.themoviedb.R

fun ImageView.setImage(url:String){
    Glide.with(context!!)
        .setDefaultRequestOptions(RequestOptions().timeout(10000))
        .load(url)
        .error(R.color.colorPrimaryDark)
        .into(this)
}

fun Int.toPx() = (this * getSystem().displayMetrics.density).toInt()