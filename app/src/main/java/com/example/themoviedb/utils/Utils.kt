package com.example.themoviedb.utils

import android.content.res.Resources

fun getStatusBarHeight(resources: Resources): Int {
    var height = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        height = resources.getDimensionPixelSize(resourceId)
    }
    return height
}
